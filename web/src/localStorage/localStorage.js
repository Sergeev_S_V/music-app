export const saveUser = user => {
  try {
    const serializedUser = JSON.stringify(user);
    localStorage.setItem('user', serializedUser);
  } catch (e) {
    console.log('Could not save state');
  }
};

export const loadUser = () => {
  try {
    const serializedUser = localStorage.getItem('user');

    if (serializedUser === null) {
      return undefined;
    }

    return JSON.parse(serializedUser);
  } catch (e) {
    return undefined;
  }
};