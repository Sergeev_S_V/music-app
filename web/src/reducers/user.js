
const initialState = {
  user: null,
	errors: {
  	loginError: null,
		registerError: null
	}
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default userReducer;