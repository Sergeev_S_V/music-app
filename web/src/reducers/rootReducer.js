import {combineReducers} from "redux/index";
import {routerReducer} from 'react-router-redux';

import artistsReducer from "./artists";
import userReducer from "./user";

export default combineReducers({
	user: userReducer,
	artists: artistsReducer,
	routing: routerReducer
});