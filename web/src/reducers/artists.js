
const initialState = {
  artists: [],
};

const artists = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default artists;