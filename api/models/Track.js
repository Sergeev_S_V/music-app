const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackSchema = new Schema({
  title: {
    type: String,
    required: true,
    unique: true,
	  validate: {
		  validator: async function (value) {
			  const track = await Track.findOne({title: value});
			  if (track) throw Error('Such track`s title already exist');
			  return true;
		  },
		  message: 'Such track`s title already exist'
	  }
  },
  album: {
    type: Schema.Types.ObjectId,
    ref: 'Album',
    required: true
  },
  duration: {
    type: String,
    required: true
  },
  number: {
    type: Number,
    required: true
  },
  youtubeLink: {
    type: String,
    unique: true
  },
  isPublished: {
    type: Boolean,
    default: false
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;