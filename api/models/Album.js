const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
  title: {
    type: String,
    required: true,
    unique: true,
	  validate: {
		  validator: async function (value) {
			  const album = await Album.findOne({title: value});
			  if (album) throw Error('Such album`s title already exist');
			  return true;
		  },
		  message: 'Such album`s title already exist'
	  }
  },
  artist: {
    type: Schema.Types.ObjectId,
    ref: 'Artist',
    required: true,
  },
  releaseDate: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  isPublished: {
    type: Boolean,
    default: false
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;