const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true,
	  validate: {
		  validator: async function (value) {
			  const artist = await Artist.findOne({name: value});
			  if (artist) throw Error('Such artist already exist');
			  return true;
		  },
		  message: 'Such artist already exist'
	  }
  },
  image: {
    type: String,
    required: true,
  },
  info: {
    type: String,
    required: true,
  },
  isPublished: {
    type: Boolean,
    default: false
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;