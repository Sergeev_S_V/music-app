const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const users = require('./app/users/users');
const artists = require('./app/artists/artists');
const albums = require('./app/albums/albums');
const tracks = require('./app/tracks/tracks');
const trackHistory = require('./app/trackHistory/trackHistory');

const config = require('./config');

const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name, {useNewUrlParser: true});

const db = mongoose.connection;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/api/users', users());
  app.use('/api/artists', artists());
  app.use('/api/albums', albums());
  app.use('/api/tracks', tracks());
  app.use('/api/trackHistory', trackHistory());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  })
});