const express = require('express');
const moment = require('moment');

const TrackHistory = require('../../models/TrackHistory');
const auth = require('../../middleware/auth');
const permit = require('../../middleware/permit');
const sortByDate = require('./sortByDate');

const createRouter = () => {
  const router = express.Router();

  router.get('/', [auth, permit('user')], async (req, res) => {
    // если зашёл админ отправлять ему все истории по каждому юзеру в убывающем порядке
    // т.е. это будут разные истории в убывающем порядке
    const { user } = req;

    try {
      const trackHistory = await TrackHistory
        .find({user: user._id, isDeleted: false})
        .populate({
          path: 'track',
          populate: {path: 'album', populate: {path: 'artist'}}
        });

      const sortedTrackHistory = sortByDate(trackHistory);

      return res.status(200).send({trackHistory: sortedTrackHistory});
    } catch (error) {
      return res.status(500).send({error: 'Something wrong happened, try later'});
    }
  });

  router.post('/', [auth, permit('user')], async (req, res) => {
    // историю админа не сохранять
    const {
      user,
      body: { track }
    } = req;

    const trackHistoryData = {
      user: user._id,
      track: track,
      datetime: moment().format('DD MM YY в hh mm')
    };

    const newTrackHistory = new TrackHistory(trackHistoryData);

    try {
      await newTrackHistory.save();
      return res.sendStatus(200);
    } catch (error) {
      return res.status(400).send({error: track + ' is not correct'});
    }

  });

  return router;
};

module.exports = createRouter;