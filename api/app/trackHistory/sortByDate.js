module.exports = array => {
  return array.sort((a,b) => a.datetime < b.datetime);
};