const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Album = require('../../models/Album');
const Track = require('../../models/Track');
const TrackHistory = require('../../models/TrackHistory');
const auth = require('../../middleware/auth');
const permit = require('../../middleware/permit');
const config = require('../../config');
const sortByRelease = require('./sortByRelease');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  router.get('/', async (req, res) => {
    const {
      query: { artist, forNewEntity, userId }
    } = req;

    if (artist) {
      try {
        let albums = await Album.find({artist, isDeleted: false});
        const sortedAlbums = sortByRelease(albums);
        if (forNewEntity) {
	        albums = albums.map(x => ({_id: x._id, title: x.title}))
        }
        return res.status(200).send({albums: forNewEntity ? albums : sortedAlbums});
      } catch (error) {
        return res.status(400).send({error: artist + ' is not correct'});
      }
    }

    try {
      const albums = await Album.find({isDeleted: false, user: userId && userId});
      const sortedAlbums = sortByRelease(albums);
      return res.status(200).send({albums: sortedAlbums});
    } catch (error) {
      return res.status(500).send({error: 'Something wrong happened, try later'});
    }
  });

  router.post('/', [auth, permit('user', 'admin'), upload.single('image')], async (req, res) => {
    const {
      body: { title, artist, releaseDate },
      file: image
    } = req;

    const albumData = {
      title,
      artist,
      releaseDate,
      image
    };

    const newAlbum = new Album(albumData);

    try {
      const album = await newAlbum.save();
      return res.status(200).send({album, message: 'Альбом успешно добавлен'});
    } catch (e) {
	    const error = e ? e.errors.name.message : 'All fields is required';
	    return res.status(400).send({error});
    }
  });

  router.patch('/:id/publish', [auth, permit('admin')], async (req, res) => {
    const {
      params: { id }
    } = req;

    try {
      await Album.findByIdAndUpdate(id, {$set: {isPublished: true}});
      return res.status(200).send({message: 'Альбом успешно опубликован', isPublished: true});
    } catch (error) {
      return res.status(400).send({error: id + ' is not correct'});
    }
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const {
      params: { id }
    } = req;

    try {
      await Album.findByIdAndUpdate(id, {$set: {isDeleted: true}});
	    await Track.update({album: id}, {isDeleted: true}, {multi: true});
	    const tracks = await Track.find({album: id});
	    const tracksIds = tracks.map(x => x._id);
	    await TrackHistory.update({track: {$in: tracksIds}}, {isDeleted: true}, {multi: true});
	    return res.status(200).send({message: 'Альбом успешно удален', isDeleted: true});
    } catch (error) {
      return res.send(400).send({error: id + ' is not correct'});
    }
  });

  return router;
};

module.exports = createRouter;