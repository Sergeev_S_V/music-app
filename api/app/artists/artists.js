const express = require('express');
const multer = require("multer");
const nanoid = require("nanoid");
const path = require('path');
const auth = require('../../middleware/auth');
const permit = require('../../middleware/permit');

const Artist = require('../../models/Artist');
const Album = require('../../models/Album');
const Track = require('../../models/Track');
const TrackHistory = require('../../models/TrackHistory');
const config = require('../../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});
const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  // добавить к исполнителям поле - биография,
  // где будет храниться схема -
  // BiographySchema(bornYear, city, country, diedYear, firstName, lastName, liveIn, info)
  router.get('/', async (req, res) => {
	  const {
	  	query: { forNewEntity, userId }
	  } = req;

    try {
      let artists = await Artist.find({isDeleted: false, user: userId && userId});
      if (forNewEntity) {
      	 artists = artists.map(x => ({_id: x._id, name: x.name}));
      }
      return res.status(200).send({artists});
    } catch (error) {
      return res.status(500).send({error: 'Something wrong happened, try later'});
    }
  });

  router.post('/', [auth, permit('user', 'admin'), upload.single('image')], async (req, res) => {
    const {
      body: {name, info},
      file: image,
    } = req;

    const artistData = {
      name,
      info,
      image
    };

    const newArtist = new Artist(artistData);

    try {
      const artist = await newArtist.save();
      return res.status(200).send({artist, message: 'Исполнитель успешно добавлен'});
    } catch (e) {
	    const error = e ? e.errors.name.message : 'All fields is required';
	    return res.status(400).send({error});
    }
  });

  router.patch('/:id/publish', [auth, permit('admin')], async (req, res) => {
    const {
      params: { id }
    } = req;

    try {
      await Artist.findByIdAndUpdate(id, {$set: {isPublished: true}});
      return res.status(200).send({message: 'Исполнитель успешно опубликован', isPublished: true});
    } catch (error) {
      return res.status(400).send({error: id + ' is not correct'});
    }
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const {
      params: { id }
    } = req;

    try {
      await Artist.findByIdAndUpdate(id, {$set: {isDeleted: true}});
      await Album.update({artist: id}, {isDeleted: true}, {multi: true});
      const albums = await Album.find({artist: id});
      const albumsIds = albums.map(x => x._id);
      await Track.update({album: {$in: albumsIds}}, {isDeleted: true}, {multi: true});
      const tracks = await Track.find({album: {$in: albumsIds}});
      const tracksIds = tracks.map(x => x._id);
      await TrackHistory.update({track: {$in: tracksIds}}, {isDeleted: true}, {multi: true});
      return res.status(200).send({message: 'Исполнитель успешно удален', isDeleted: true});
    } catch (error) {
      return res.send(400).send({error: id + ' is not correct'});
    }
  });

  return router;
};

module.exports = createRouter;
