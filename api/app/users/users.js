const express = require('express');

const User = require('../../models/User');
const auth = require('../../middleware/auth');
const permit = require('../../middleware/permit');

const createRouter = () => {
  const router = express.Router();

  router.get('/', [auth, permit('admin')], async (req, res) => {
    try {
      const users = await User.find();
      return res.status(200).send({users});
    } catch (error) {
      return res.status(500).send({error});
    }
  });

  router.post('/', async (req, res) => {
    const {
      body: { username, password }
    } = req;

    const userData = { username, password };
    const user = new User(userData);

    try {
      await user.save();
      return res.status(200).send({message: 'Регистрация прошла успешно'});
    } catch (e) {
	    const error = e ? e.errors.name.message : 'Server error, try later';
	    const status = e ? 400 : 500;
	    return res.status(status).send({error});
    }
  });

  router.post('/sessions', async (req, res) => {
    const {
      body: { username, password }
    } = req;
    const user = await User.findOne({username});

    if (!user) {
      return res.status(400).send({error: 'Username not found'});
    }

    const isMatch = await user.checkPassword(password);

    if (!isMatch) {
      return res.status(400).send({error: 'Password is wrong!'});
    }

    user.generateToken();
    await user.save();

    return res.status(200).send(user);
  });

  router.delete('/sessions', async (req, res) => {
    const token = req.get('Token');
    const success = {message: 'Logout success!'};

    if (!token) return res.status(200).send(success);

    const user = await User.findOne({token});

    if (!user) return res.status(200).send(success);

    user.generateToken();
    user.save();

    return res.status(200).send(success);
  });

  return router;
};

module.exports = createRouter;