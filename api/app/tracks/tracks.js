const express = require('express');

const Track = require('../../models/Track');
const Album = require('../../models/Album');
const TrackHistory = require('../../models/TrackHistory');
const auth = require('../../middleware/auth');
const permit = require('../../middleware/permit');

const router = express.Router();

const sortByNumber = tracks => { // не сортирует
  return tracks.sort((a, b) => a.number - b.number);
};

const createRouter = () => {
  router.get('/', async (req, res) => {
    const {
      query: { album, artist, userId }
    } = req;

    if (album) {
      try {
        const tracks = await Track.find({album, isDeleted: false});
        return res.status(200).send({tracks});
      } catch (error) {
        return res.status(400).send({error: album + ' is not correct'});
      }
    }

    if (artist) {
      try {
        const albums = await Album.find({artist, isDeleted: false});
        const ids = albums.map(x => x._id);
        const tracks = await Track.find({album: {$in: ids}, isDeleted: false});
        return res.status(200).send({tracks});
      } catch (error) {
        return res.status(400).send({error: artist + ' is not correct'});
      }
    }

    try {
      const tracks = await Track.find({isDeleted: false, user: userId && userId});
      return res.status(200).send({tracks});
    } catch (error) {
      return res.status(500).send({error: 'Something wrong happened, try later'});
    }
  });

  router.post('/', [auth, permit('user', 'admin')], async (req, res) => {
    const {
      body: { title, album, duration, youtubeLink }
    } = req;

    const trackData = {
      title,
      album,
      duration,
      youtubeLink
    };

    const newTrack = new Track(trackData);

    try {
      const track = await newTrack.save();
      return res.status(200).send({track, message: 'Трэк успешно добавлен'});
    } catch (e) {
	    const error = e ? e.errors.name.message : 'All fields is required';
	    return res.status(400).send({error});
    }
  });

  router.patch('/:id/publish', [auth, permit('admin')], async (req, res) => {
    const {
      params: { id }
    } = req;

    try {
      await Track.findByIdAndUpdate(id, {$set: {isPublished: true}});
      return res.status(200).send({message: 'Трэк успешно опубликован', isPublished: true});
    } catch (error) {
      return res.status(400).send({error: id + ' is not correct'});
    }
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const {
      params: { id }
    } = req;

    try {
      await Track.findByIdAndUpdate(id, {$set: {isDeleted: true}});
	    await TrackHistory.update({track: id}, {isDeleted: true}, {multi: true});
	    return res.status(200).send({message: 'Трэк успешно удален', isDeleted: true});
    } catch (error) {
      return res.send(400).send({error: id + ' is not correct'});
    }
  });

  return router;
};

module.exports = createRouter;
